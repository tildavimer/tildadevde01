"""Download .fits files from the ALMA Archive based on keywords. Uses a modified version of
   ALminer's own function download_data in order to pick out 500 random images from the 
   query. There is a cleanup script added at the end in order to delete "extra" .pickle files
   coming with the .fits files due to Python as well as larger files than 30 MB."""

import alminer
import pandas as pd
import os, os.path

#'"disks around low-mass stars"' is not working and has been published as an issue at the code provider.

# TILL ALEX
# Testa att lägg till disks around low-mass stars på samma sätt som de keywordsen nedanför.
# Ifall du vill ladda ner givet de olika filtreringarna via keywordsen kan du låta listan nedanför fyllas 
# med ett keyword i taget, alltså såhär: ['"intermediate-mass star formation"']

# Just nu så laddar du ner direkt ifall du väljer att köra filen, ifall du vill ändra det till att
# enbart displaya vad som kommer tankas ned kan du ändra dryrun till True på rad 37.

#DIR = './fits/1'  # 1
#KEYWORDS = ['"intermediate-mass star formation"']  # 1
#DIR = './fits/2'  # 2
#KEYWORDS = ['"outflows, jets, feedback"']  # 2

#DIR = './fits/3'  # 3
#KEYWORDS = ['"outflows, jets and ionized winds"']  # 3

DIR = './fits/4'  # 4
KEYWORDS = ['"inter-stellar medium (ism)/molecular clouds"']  # 4

def download_routine(datadir, keywords):
    obs_holder = pd.DataFrame()
    for i in range(len(keywords)):
        # Query and filtering
        print(" Querying with keyword : " + keywords[i])
        my_query = alminer.keysearch({'science_keyword':[keywords[i]]}, print_targets=False, tap_service='NAOJ')
        selected = my_query[my_query.obs_release_date > '2016']
        selected = selected[selected.ang_res_arcsec < 0.4]
        selected = selected.drop_duplicates(subset='obs_id').reset_index(drop=True)
        obs_holder = pd.concat([obs_holder, selected])

    print("Proceeding to download fits files from the following dataframe.")
    print(obs_holder)
    #obs_holder = obs_holder.sample(10)
    print(alminer.summary(obs_holder, print_targets=False))
    #alminer.download_data(obs_holder, fitsonly=True, dryrun=True, location=datadir, filename_must_include=[".pbcor", "_sci", ".cont"], archive_mirror='NAOJ')
    alminer.download_data(obs_holder, fitsonly=True, dryrun=False, location=datadir, filename_must_include=[".pbcor", "_sci", ".cont"], archive_mirror='NAOJ')

    return


# Funktionen nedanför är för att ta bort större filer samt de picklefiler som kommer med nedladdningen
# Vet ej varför picklefilerna finns där, kanske du kan svara mig på imorgon?
def cleanup(dir):
    for root, _, files in os.walk(dir):
        for f in files:
            fullpath = os.path.join(root, f)
            if f.endswith(('.pickle')):
                os.remove(fullpath)
#            elif os.path.getsize(fullpath) > 30 * 1024 * 1000:
#            os.remove(fullpath)

download_routine(DIR, KEYWORDS)
cleanup(DIR)
